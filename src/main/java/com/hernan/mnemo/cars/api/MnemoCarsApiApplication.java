package com.hernan.mnemo.cars.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MnemoCarsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MnemoCarsApiApplication.class, args);
    }

}
